import './App.css';
import React, { useState } from "react";
import { Chart } from "react-google-charts";

export const tempCandlestickChartData = [
  ["date", "o", "h", "l", "c"],
  [new Date(2020, 0), 20, 28, 38, 45],
  [new Date(2022, 0), 31, 38, 55, 66],
  [new Date(2010, 0), 50, 55, 77, 80],
];

export const candlestickChartOptions = {
  legend: "none",
};

export const tempComboChartData = [
  ["Element", "Density"],
  ["Copper", 8.94],
  ["Silver", 10.49],
  ["Gold", 19.3],
  ["Platinum", 21.45],
];

export const comboChartOptions = {
  vAxis: { title: "Volume" },
  hAxis: { title: "Date" },
  seriesType: "bars",
  series: { 1: { type: "line" }, 2: { type: "line" } },
};

function App() {
  const [stockTicker, setStockTicker] = useState("")
  const [startDate, setStartDate] = useState("")
  const [endDate, setEndDate] = useState("")

  const [candlestickChartData, setCandlestickChartData] = useState(tempCandlestickChartData)
  const [comboChartData, setComboChartData] = useState(tempComboChartData)

  function handleClick() {
      fetch(buildUrl(stockTicker, startDate, endDate))
      .then(response => response.json())
      .then(data => {
        setCandlestickChartData(mapResponseToCandlestickChart(data));
        setComboChartData(mapResponseToComboChart(data));
      })
  }

  return (
    <div>
      <form>
        <label for="stockTicker">Stock Ticker</label><br />
        <input value={stockTicker} onChange={e => setStockTicker(e.target.value)}
          type="text" id="stockTicker" name="stockTicker" /><br />
        <label for="startDate">Start Date</label><br />
        <input value={startDate} onChange={e => setStartDate(e.target.value)}
          type="date" id="startDate" name="startDate" /><br />
        <label for="endDate">End Date</label><br />
        <input value={endDate} onChange={e => setEndDate(e.target.value)}
          type="date" id="endDate" name="endDate" /><br />
      </form>
      <button onClick={handleClick}>Refresh charts</button>

      <Chart
        chartType="CandlestickChart"
        width="70%"
        height="400px"
        data={candlestickChartData}
        options={candlestickChartOptions}
      />

      <Chart 
        chartType="ComboChart"
        width="70%" 
        height="400px" 
        data={comboChartData}
        options={comboChartOptions}
      />
    </div>
  );

  function mapResponseToCandlestickChart(response) {
    return [["date", "o", "h", "l", "c"]]
    .concat(response.map(e => [new Date(e.date), e.open, e.high, e.low, e.close]));
  }

  function mapResponseToComboChart(response) {
    return [["Year", "Volume", "5 day moving average", "20 day moving average"]]
    .concat(response.map(e => [new Date(e.date), e.numberOfShares, e.fiveDayMovingAverage, e.twentyDayMovingAverage]));
  }

  function buildUrl(stockTicker, startDate, endDate) {
    const queryParams = new URLSearchParams();
    if (!!startDate) {
      queryParams.append('start_date', startDate)
    }
    if (!!endDate) {
      queryParams.append('end_date', endDate)
    }
    const queryParamsString = queryParams.size === 0 ? '' : '?' + queryParams.toString();
    return `http://localhost:8080/api/v1/nasdaq-controller/${stockTicker}` + queryParamsString;
  }
}

export default App;
